{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs-for-mesa.url = "github:NixOS/nixpkgs/40af042f60f52557af9d4692a60d6da05b27ef1d";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    nixpkgs-for-mesa,
  }: let
    pythonOverlay = final: prev: {
      clang = final.callPackage ./libclang-python.nix {};

      casadi = final.callPackage ./casadi.nix {};

      ocp = final.callPackage ./ocp.nix {
        inherit (final.pkgs.darwin.apple_sdk.frameworks) Cocoa;
      };

      cadquery = final.callPackage ./cadquery.nix {};

      nlopt = final.toPythonModule final.pkgs.nlopt;

      spyder = prev.spyder.overridePythonAttrs (old: {
        postPatch = ''
          sed -i -e 's/import applaunchservices.*/pass/' spyder/utils/qthelpers.py
        '';

        meta =
          old.meta
          // {
            platforms = final.lib.platforms.all;
          };
      });
      qtawesome = prev.qtawesome.overridePythonAttrs (old: {
        meta =
          old.meta
          // {
            platforms = final.lib.platforms.all;
          };
      });
    };

    overlay = final: prev: {
      inherit (import nixpkgs-for-mesa {inherit (prev) system;}) mesa mesa_glu;

      # avoid having two VTKs installed that are the same.
      # on MacOS prevents:
      #   objc[N]: Class vtk... is implemented in both [...] and [...].
      #   One of the two will be used. Which one is undefined.
      inherit (final.python3.pkgs) vtk;

      pywrap = final.callPackage ./pywrap.nix {};
      opencascade-occt = final.callPackage ./opencascade-occt.nix {};

      python3 = prev.python3.override {packageOverrides = pythonOverlay;};

      python3Packages = final.python3.pkgs;

      nlopt = prev.nlopt.overrideAttrs (old: {
        buildInputs = old.buildInputs ++ [final.python3 final.swig];
        propagatedBuildInputs = [final.python3.pkgs.numpy];

        # build system is based on cmake, why is this here?
        configureFlags = [];
      });

      cq-editor = prev.cq-editor.overrideAttrs (old: {
        version = "unstable-20230826";
        patches = [];
        src = final.fetchFromGitHub {
          owner = "CadQuery";
          repo = "CQ-editor";
          rev = "181adb99de9aa8cecba105069502be491e927342";
          hash = "sha256-PbCBSaRFbFzMo2m1w4A2UpAGu8q0IX8V4agNiVQmpSM=";
        };

        postPatch = ''
          perl -i -ne 'print if !/CQ-editor = /' setup.py
        '';

        nativeBuildInputs = old.nativeBuildInputs ++ [final.perl];
      });
    };

    systemFn = system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [overlay];
      };
    in {
      packages.cq-editor = pkgs.cq-editor;
      packages.cadquery = pkgs.python3.pkgs.cadquery;
      devShells.default = (pkgs.python3.withPackages (ps: with ps; [cadquery])).env.overrideAttrs (old: {nativeBuildInputs = old.nativeBuildInputs ++ [pkgs.cq-editor];});
    };
  in
    flake-utils.lib.eachDefaultSystem systemFn;
}
