{
  toPythonModule,
  stdenv,
  fetchFromGitHub,
  cmake,
  ninja,
  pkg-config,
  python3,
  swig,
}:
toPythonModule (
  stdenv.mkDerivation rec {
    pname = "casadi";
    version = "3.6.3";

    src = fetchFromGitHub {
      owner = "casadi";
      repo = "casadi";
      rev = version;
      hash = "sha256-l5BlZAAPvF/XS9+w9ZIhhKpuGw4Fx6G5ivxdtLdmkwU=";
    };

    patches = [
      ./0001-casadi-no-apple-workaround.patch
    ];

    cmakeFlags = [
      # the cmake package does not handle absolute CMAKE_INSTALL_INCLUDEDIR correctly
      # (setting it to an absolute path causes include files to go to $out/$out/include,
      #  because the absolute path is interpreted with root at $out).
      "-DCMAKE_INSTALL_INCLUDEDIR=include"
      "-DCMAKE_INSTALL_LIBDIR=lib"
      "-DWITH_PYTHON=ON"
      "-DWITH_PYTHON3=ON"
      "-DWITH_SELFCONTAINED=ON"
      "-DLIB_PREFIX=${placeholder "out"}/lib"
      "-DBIN_PREFIX=${placeholder "out"}/bin"
      "-DINCLUDE_PREFIX=${placeholder "out"}/include"
      "-DCMAKE_PREFIX=${placeholder "out"}/cmake"
      "-DPYTHON_PREFIX=${placeholder "out"}/${python3.sitePackages}"
      "-DCASADI_PYTHON_PIP_METADATA_INSTALL=ON"
    ];

    nativeBuildInputs = [cmake ninja pkg-config swig python3];

    postInstall = ''
      rm $out/include/tinyxml2.h
    '';

    propagatedBuildInputs = [python3 python3.pkgs.numpy];
  }
)
