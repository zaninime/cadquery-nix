{
  lib,
  stdenv,
  fetchFromGitHub,
  cmake,
  ninja,
  opencascade-occt,
  pywrap,
  libclang,
  libcxx,
  vtk,
  rapidjson,
  libGL,
  python3,
  toPythonModule,
  sd,
  Cocoa,
}: let
  pname = "ocp";
  version = "${opencascade-occt.version}.0";

  generatedSources = stdenv.mkDerivation {
    pname = "${pname}-sources";
    inherit version;

    src = fetchFromGitHub {
      owner = "CadQuery";
      repo = "OCP";
      rev = version;
      hash = "sha256-4nZad/5hF6MABdK8wbQS7iqjJFBWtNqubgpd1WcZ0aw=";
    };

    nativeBuildInputs = [pywrap sd];

    pywrapPlatform = with stdenv.targetPlatform;
      if isLinux
      then "Linux"
      else if isMacOS
      then "OSX"
      else if isWindows
      then "Windows"
      else throw "Unsupported platform";

    patchPhase =
      ''
        sd 'input_folder.*' 'input_folder = "${opencascade-occt}/include/opencascade"' ocp.toml
        mkdir -p include
      ''
      + lib.optionalString stdenv.isDarwin ''
        ln -s ${libGL.dev}/include/GL include/OpenGL
      '';

    buildPhase = ''
      pywrap \
        -n $NIX_BUILD_CORES \
        -i ${libcxx.dev}/include/c++/v1/ \
        -i ${libclang.lib}/lib/clang/${lib.getVersion libclang}/include/ \
        -i ${stdenv.cc.libc}/include/ \
        -i ${rapidjson}/include/ \
        -i ${libGL.dev}/include/ \
        -i include/ \
        -i ${vtk}/include/vtk/ \
        -l ${libclang.lib}/lib/libclang.dylib \
        all ocp.toml $pywrapPlatform
    '';

    installPhase = ''
      cp -r OCP $out
    '';
  };
in
  toPythonModule (
    stdenv.mkDerivation {
      inherit pname version;
      src = generatedSources;

      nativeBuildInputs = [ninja];

      buildInputs =
        [
          python3.pkgs.pybind11
          cmake
          rapidjson
          libGL
          opencascade-occt
        ]
        ++ lib.optionals stdenv.isDarwin [Cocoa];

      propagatedBuildInputs = [
        python3.pkgs.vtk
      ];

      extraCmakeLists = ''
        find_package(rapidjson REQUIRED)
        target_include_directories(OCP PRIVATE ${opencascade-occt}/include/opencascade ''${rapidjson_INCLUDE_DIRS})
        target_link_libraries(OCP PRIVATE ''${rapidjson_LIBRARIES})
      '';

      passAsFile = ["extraCmakeLists"];

      preConfigure = ''
        echo >> CMakeLists.txt
        cat $extraCmakeListsPath >> CMakeLists.txt
      '';

      postInstall = ''
        mkdir -p $out/${python3.sitePackages}
        mv $out/site-packages/OCP* $out/${python3.sitePackages}/
      '';
    }
  )
