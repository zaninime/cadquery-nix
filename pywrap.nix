{
  python3,
  fetchFromGitHub,
}:
python3.pkgs.buildPythonApplication {
  pname = "pywrap";
  version = "unstable-20230210";

  src = fetchFromGitHub {
    owner = "CadQuery";
    repo = "pywrap";
    rev = "f3bcde70fd66a2d884fa60a7a9d9f6aa7c3b6e16";
    hash = "sha256-QhAvJHV5tFq9bjKOzEpcudZNnmUmNVrJ+BLCZJhO31g=";
  };

  patches = [
    ./0001-pywrap-pandas-compat.patch
  ];

  propagatedBuildInputs = with python3.pkgs; [
    schema
    click
    logzero
    path
    clang
    toml
    pandas
    joblib
    tqdm
    jinja2
    toposort
    pyparsing
    pybind11
  ];
}
