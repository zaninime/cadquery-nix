{
  buildPythonPackage,
  llvmPackages,
  writeText,
  setuptools,
}:
buildPythonPackage rec {
  pname = "libclang";
  inherit (llvmPackages.libclang) version src;

  postUnpack = let
    pyproject_toml = writeText "pyproject.toml" ''
      [build-system]
      requires = [
          "setuptools>=42",
          "wheel"
      ]
      build-backend = "setuptools.build_meta"
    '';
    setup_cfg = writeText "setup.cfg" ''
      [metadata]
      name = clang
      version = ${version}

      [options]
      packages = clang
    '';
  in ''
    sourceRoot="$sourceRoot/bindings/python"
    for _src in "${pyproject_toml}" "${setup_cfg}"; do
      cp "$_src" "$sourceRoot/$(stripHash "$_src")"
    done
  '';

  format = "pyproject";

  nativeBuildInputs = [setuptools];
}
