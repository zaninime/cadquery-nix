{
  lib,
  stdenv,
  fetchurl,
  fetchpatch,
  cmake,
  ninja,
  tcl,
  tk,
  libGL,
  libGLU,
  libXext,
  libXmu,
  libXi,
  darwin,
  vtk,
  rapidjson,
  freeimage,
}:
stdenv.mkDerivation rec {
  pname = "opencascade-occt";
  version = "7.7.1";
  commit = "V${builtins.replaceStrings ["."] ["_"] version}";

  src = fetchurl {
    name = "occt-${commit}.tar.gz";
    url = "https://git.dev.opencascade.org/gitweb/?p=occt.git;a=snapshot;h=${commit};sf=tgz";
    sha256 = "sha256-piIJ/gIlmnbyIz1c/rWhasa3IeSJ2LHugRhlZUS65JY=";
  };

  # from https://github.com/conda-forge/occt-feedstock/blob/main/recipe/build.sh
  cmakeFlags = [
    "-DBUILD_MODULE_Draw:BOOL=OFF"
    "-DUSE_TBB:BOOL=OFF"
    "-DBUILD_RELEASE_DISABLE_EXCEPTIONS:BOOL=OFF"
    "-DUSE_VTK:BOOL=ON"
    "-D3RDPARTY_VTK_LIBRARY_DIR:FILEPATH=${vtk}/lib"
    "-D3RDPARTY_VTK_INCLUDE_DIR:FILEPATH=${vtk}/include"
    "-DVTK_RENDERING_BACKEND:STRING=OpenGL2"
    "-DUSE_FREEIMAGE:BOOL=ON"
    "-DUSE_RAPIDJSON:BOOL=ON"
  ];

  nativeBuildInputs = [cmake ninja];
  buildInputs =
    [tcl tk vtk rapidjson freeimage libGL libGLU libXext libXmu libXi]
    ++ lib.optional stdenv.isDarwin darwin.apple_sdk.frameworks.Cocoa;

  meta = with lib; {
    description = "Open CASCADE Technology, libraries for 3D modeling and numerical simulation";
    homepage = "https://www.opencascade.org/";
    license = licenses.lgpl21; # essentially...
    # The special exception defined in the file OCCT_LGPL_EXCEPTION.txt
    # are basically about making the license a little less share-alike.
    maintainers = with maintainers; [amiloradovsky gebner];
    platforms = platforms.all;
  };
}
