{
  lib,
  buildPythonPackage,
  fetchFromGitHub,
  pyparsing,
  pytest,
  makeFontsConf,
  freefont_ttf,
  casadi,
  path,
  typish,
  ezdxf,
  nptyping,
  multimethod,
  ocp,
  nlopt,
}:
buildPythonPackage rec {
  pname = "cadquery";
  version = "2.3.1";

  src = fetchFromGitHub {
    owner = "CadQuery";
    repo = pname;
    rev = version;
    sha256 = "sha256-ibQnf1eevyuZ6+w3/KB16HMUbk5u8hMfFcwKdzNUcug=";
  };

  CONDA_PREFIX_1 = "x";

  propagatedBuildInputs = [
    ocp
    pyparsing
    path
    casadi
    typish
    ezdxf
    nptyping
    multimethod
    nlopt
  ];

  FONTCONFIG_FILE = makeFontsConf {
    fontDirectories = [freefont_ttf];
  };

  doCheck = false;
  nativeCheckInputs = [
    pytest
  ];

  meta = with lib; {
    description = "Parametric scripting language for creating and traversing CAD models";
    homepage = "https://github.com/CadQuery/cadquery";
    license = licenses.asl20;
    maintainers = with maintainers; [marcus7070];
  };
}
